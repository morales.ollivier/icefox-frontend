import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Layout, Menu, Icon, Typography } from 'antd';

const { Sider } = Layout;
const { Title } = Typography;

class Sidebar extends Component {
    render() {
        return (
          <Sider style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }} >
            <div className="logo" >
              <Title level={2} style={{textAlign:'center'}} type="warning" >Icefox Logo</Title>
            </div>
            <Menu theme="dark" mode="inline">
              <Menu.Item key='1'>
                <Link to='/'>
                  <Icon type='home' />
                  <span className='nav-text'>Home</span>
                </Link>
                </Menu.Item>
              <Menu.Item key='2'>
                <Link to='/listingpage'>
                  <Icon type="unordered-list" />
                  <span className='nav-text'>Listing Page</span>
                  </Link>
                </Menu.Item>
            </Menu>
          </Sider>
    );
  }
}

export default Sidebar;
