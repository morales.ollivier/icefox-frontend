import React from 'react'
import { Layout } from 'antd';

const {  Footer } = Layout;

const MyFooter = () => {
	return (
		<Layout style={{ marginLeft: 200 }}>
			<Footer style={{ textAlign: 'center', backgroundColor: '#69c0ff', position: 'fixed', bottom: 0, width:"200vh" }}>IceFox Tech. Inc. ©2018 Created by Ollivier Julles Morales</Footer>
		</Layout>
	);
}

export default MyFooter;
