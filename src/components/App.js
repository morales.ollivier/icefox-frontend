import React, { Component,Fragment } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

// components
import Sidebar from './Sidebar';
import MyFooter from './MyFooter';
import ListingPage from './ListingPage';

class App extends Component {
    render() {
      return (
        <Fragment>
            <BrowserRouter>
                <Sidebar />
                <Switch>
                    <Route exact path='/listingpage' component={ ListingPage } />
                </Switch>
            </BrowserRouter>
            <MyFooter />
        </Fragment>
      );
    }
  }
  
  export default App;