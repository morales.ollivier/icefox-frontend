//dummydata
import data from '../data/dummydata';

import React, { Component } from 'react';
import Moment from 'moment';
import { extendMoment } from 'moment-range';

import { Layout, Icon, Table  } from 'antd';

const moment = extendMoment(Moment);
const { Content } = Layout;

class ListingPage extends Component {

	state = {
	    filteredInfo: null,
	    sortedInfo: null,
 	};

 	handleChange = (pagination, filters, sorter) => {
	    console.log('Various parameters', pagination, filters, sorter);
	    this.setState({
	      filteredInfo: filters,
	      sortedInfo: sorter,
	    });
  	};

	render() {

		let { sortedInfo, filteredInfo } = this.state;
	    sortedInfo = sortedInfo || {};
	    filteredInfo = filteredInfo || {};
	    let dateToday = moment(new Date()).format("L");
		const start = moment().subtract(1, 'weeks').startOf('isoWeek').format('L');
		const end   = moment().subtract(1, 'weeks').endOf('isoWeek').format('L');
		// const dates = [moment('2011-04-15', 'YYYY-MM-DD'), moment('2011-11-27', 'YYYY-MM-DD')];
		// const range = moment.range(dates);
		const dates = [start, end]
		const range = moment.range(dates);
		
		console.log(range)
		// console.log(range.toDate());

	    const columns = [
	      {
	        title: 'Date time',
	        dataIndex: 'datetime',
	        key: 'datetime',
	        filters: [{ text: 'Today', value: dateToday }, { text: 'Yesteday', value: moment().subtract(1, 'days').format('L')}, { text: 'Last Week', value: start }, {text: 'This Month', value: moment().format('MM')}],
	        filteredValue: filteredInfo.datetime || null,
	        onFilter: (value, record) => record.datetime.includes(value),
	        sorter: (a, b) => a.datetime.split('/').reverse().join().localeCompare(b.datetime.split('/').reverse().join()),
	        sortOrder: sortedInfo.columnKey === 'datetime' && sortedInfo.order,
	        width: 500,
	      },
	      {
	        title: 'Device',
	        dataIndex: 'device',
	        key: 'device',
	        width: 500,
	      },
	      {
	        title: 'IP',
	        dataIndex: 'IP',
	        key: 'IP',
	        width: 500,
	      },
	    ];
		return (

			<Layout style={{ marginLeft: 200 }}>
				<Content style={{ margin: '24px 16px 24px', overflow: 'initial'}}>
					<div style={{ padding: 24, background: '#fff', height: "calc(100vh - 116px)" }}>
					<h1><Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
					&nbsp;Listing Page</h1>
						<div>
					        <div className="table-operations">
					        </div>
				        	<Table columns={columns} dataSource={data} onChange={this.handleChange} />
				      	</div>
					</div>
					
				</Content>
			</Layout>
		);
	}
}

export default ListingPage;
