const data = [
    {
      key: '1',
      datetime: '08/14/2019, 11:09:39 pm',
      device: 'Desktop',
      IP: '192.168.254.201',
    },
    {
      key: '2',
      datetime: '08/05/2019, 11:09:39 pm',
      device: 'Iphone',
      IP: '192.168.254.221',
    },
    {
      key: '3',
      datetime: '08/05/2019, 11:09:39 pm',
      device: 'Laptop',
      IP: '192.168.254.207',
    },
    {
      key: '4',
      datetime:'07/05/2019, 11:09:39 pm',
      device: 'Laptop',
      IP: '192.168.254.205',
    },
  
    {
      key: '5',
      datetime: '08/22/2019, 11:09:39 pm',
      device: 'Iphone',
      IP: '192.168.254.204',
    },
  
    {
      key: '6',
      datetime: '08/21/2019, 11:09:39 pm',
      device: 'Iphone',
      IP: '192.168.254.401',
    },
  
    {
      key: '7',
      datetime: '08/25/2019, 11:09:39 pm',
      device: 'Laptop',
      IP: '192.168.254.301',
    },
  
    {
      key: '8',
      datetime: '08/22/2019, 11:09:39 pm',
      device: 'Desktop',
      IP: '192.168.223.101',
    },
  
    {
      key: '9',
      datetime: '06/01/2019, 11:09:39 pm',
      device: 'Iphone',
      IP: '192.168.155.251',
    },
  
    {
      key: '10',
      datetime: '07/02/2019, 11:09:39 pm',
      device: 'Desktop',
      IP: '192.168.3.203',
    },
  
    {
      key: '11',
      datetime: '08/13/2019, 11:09:39 pm',
      device: 'Iphone',
      IP: '192.168.2.202',
    },

    {
        key: '12',
        datetime: '08/13/2019, 11:09:39 pm',
        device: 'Laptop',
        IP: '192.168.9.202',
    },

    {
        key: '13',
        datetime: '08/13/2019, 11:09:39 pm',
        device: 'Cherry Mobile',
        IP: '192.168.7.202',
    },

    {
        key: '14',
        datetime: '08/23/2019, 11:09:39 pm',
        device: 'Android',
        IP: '192.168.8.202',
    },

    {
        key: '15',
        datetime: '08/07/2019, 11:09:39 pm',
        device: 'Desktop',
        IP: '192.168.5.202',
    },

    {
        key: '16',
        datetime: '08/06/2019, 11:09:39 pm',
        device: 'Laptop',
        IP: '192.168.7.202',
    },

    {
        key: '17',
        datetime: '08/10/2019, 11:09:39 pm',
        device: 'Iphone',
        IP: '192.168.5.202',
    },

    {
        key: '18',
        datetime: '08/15/2019, 11:09:39 pm',
        device: 'Windows',
        IP: '192.168.3.202',
    },

    {
        key: '19',
        datetime: '08/23/2019, 11:09:39 pm',
        device: 'MAC OSX',
        IP: '192.168.1.202',
    },

    {
        key: '20',
        datetime: '08/13/2019, 11:09:39 pm',
        device: 'Laptop',
        IP: '192.168.5.202',
    },

    {
        key: '21',
        datetime: '08/22/2019, 11:09:39 pm',
        device: 'Iphone',
        IP: '192.168.1.202',
    },
  ];

  export default data;